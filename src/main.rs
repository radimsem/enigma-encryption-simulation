use std::fmt::{Display, Debug};
use std::{io::stdin, vec};
use std::fs::read_to_string;

use anyhow::{Result, Ok, bail};

enum Job {
    ENCRYPT,
    DECRYPT
}

struct HashingPair {
    current: char,
    target: char
}

const HASHING_TABLES_COUNT: u8 = 6;
const ALLOWED_LETTERS: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

fn handle_valid_input(input: &str) -> Result<()> {
    for letter in input.chars().into_iter() {
        if letter.is_lowercase() {
            bail!("Letter must be uppercase!")
        } else {
            let mut allowed = false;
            ALLOWED_LETTERS.chars().into_iter().for_each(|allowed_letter| {
                if allowed_letter == letter || letter.is_whitespace() {
                    allowed = true;
                }
            });

            if !allowed {
                bail!("Character {} must be from English alphabet!", letter)
            }
        }
    }

    Ok(())
}

fn handle_config<T>(value: T, config_options: &Vec<T>) -> Result<T>
where
    T: PartialEq + Display + Debug
{
    if !config_options.into_iter().any(|option| *option == value) {
        bail!("Value {} does not match any possible configuration from options {:?}!", value, config_options)
    }

    Ok(value)
}

fn get_job_enum(job_config_value: &str, job_config_options: &Vec<&str>) -> Result<Job> {
    for (i, &val) in job_config_options.iter().enumerate() {
        if job_config_value == val {
            match i {
                0 => return Ok(Job::ENCRYPT),
                1 => return Ok(Job::DECRYPT),
                _ => bail!("Job config value {} does not match any options!", job_config_value)
            }
        }
    }

    bail!("Could not get job enum!")
}

fn remove_whitespace(text: &String) -> String {
    text.chars().filter(|c| !c.is_whitespace()).collect()
}

fn get_char_from_bytes(pair: &[u8], index: usize) -> Result<char> {
    let byte_option = pair.get(index);
    
    match byte_option {
        Some(byte) => Ok(*byte as char),
        None => bail!("Can not find letter in hashing pair!")
    }
}

fn handle_job(input: String, job: Job) -> Result<[String; 2]> {
    let mut result_input = input.clone();

    for i in 1..=HASHING_TABLES_COUNT {
        let table_content = read_to_string(format!("docs/enigma_table_{}.txt", match job {
            Job::ENCRYPT => i,
            Job::DECRYPT => HASHING_TABLES_COUNT - (i - 1)
        }))?;
        let mut hashing_pairs: Vec<HashingPair> = vec![];
        let mut new_result_handler = String::new();

        for line in table_content.lines().into_iter() {
            let mut line = remove_whitespace(&line.to_string());

            if line.ends_with(",") {
                line.remove(line.len() - 1);
            }

            let line_pairs = line.split(",");
            for pair in line_pairs {
                let pair_bytes = pair.as_bytes();
                hashing_pairs.push(HashingPair { 
                    current: get_char_from_bytes(&pair_bytes, 0)?, 
                    target: get_char_from_bytes(&pair_bytes, 1)?
                })
            }
        }

        for letter in result_input.chars().into_iter() {
            let target_hash_pair = hashing_pairs.iter().find(|pair| {
                match job {
                    Job::ENCRYPT => pair.current == letter,
                    Job::DECRYPT => pair.target == letter
                }
            });

            match target_hash_pair {
                Some(pair) => new_result_handler.push(match job {
                    Job::ENCRYPT => pair.target,
                    Job::DECRYPT => pair.current
                }),
                None => bail!("Letter {} does not match any letter in hashing table!", letter)
            }
        }

        result_input = new_result_handler;
    }

    Ok([input, result_input])
}

fn main() -> Result<()> {
    let mut input = String::new();
    let mut job = String::new();
    let job_config_options: Vec<&str> = vec!["encrypt", "decrypt"];

    println!("Enter input:");
    stdin().read_line(&mut input)?;
    handle_valid_input(&input)?;

    println!("Would you like to encrypt or decrypt the input?:");
    stdin().read_line(&mut job)?;
    
    let job_config_value = handle_config::<&str>(job.trim(), &job_config_options)?;
    let job = get_job_enum(job_config_value, &job_config_options)?;

    let results = handle_job(input.trim().to_string(), job)?;

    println!("Input: {}", results[0]);
    println!("Result: {}", results[1]);

    Ok(())
}
